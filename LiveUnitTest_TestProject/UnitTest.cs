﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using LiveUnitTest;

namespace LiveUnitTest_TestProject
{
    [TestClass]
    public class UnitTest
    {
        [TestMethod]
        public void TestMethod_HelloWorld_Greetings()
        {

            // Arrange
            string name = "Mr. Nhan Heo";
            string expected = $"Hello {name}, Welcome to Unit Test Training.";
            HelloWorld helloWorld = new HelloWorld(name);

            // Act
            string actual = helloWorld.Greetings();

            // Assert
            Assert.AreEqual(expected, actual, "Greetings not generated correctly");
        }

        [TestMethod]
        public void TestMethod_BankTransaction_Debit()
        {

            // Arrange
            string name = "Mr. Nhan Heo";
            float openingBalance = 11.99f;
            float debitAmount = 4.55f;
            float expected = 7.44f;
            BankTransaction transaction = new BankTransaction(name, openingBalance);

            // Act
            transaction.Debit(debitAmount);
            float actual = transaction.Balance;

            // Assert
            Assert.AreEqual(expected, actual, 0.001, "Account not debited correctly");
        }
    }
}
