﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LiveUnitTest
{
    class Program
    {
        static void Main(string[] args)
        {
            //TraditionalTest_HelloWorld_Greetings();
            TraditionalTest_BankTransaction_Debit();

            Console.Write("\nProcess done...");
            Console.ReadKey();
        }

        static void TraditionalTest_HelloWorld_Greetings()
        {
            Console.Write("Hello, tell me your Name: ");
            string name = Console.ReadLine();

            HelloWorld myHelloWorld = new HelloWorld(name);
            string greetings = myHelloWorld.Greetings();
            Console.WriteLine(greetings);
        }
        static void TraditionalTest_BankTransaction_Debit()
        {
            Console.Write("Hello, tell me your Name: ");
            string name = Console.ReadLine();
            Console.Write("Your Opening Balance: ");
            float openingBalance = float.Parse(Console.ReadLine());
            Console.Write("Amount you want to Withdraw: ");
            float debitAmount = float.Parse(Console.ReadLine());

            BankTransaction transaction = new BankTransaction(name, openingBalance);
            transaction.Debit(debitAmount);
            Console.WriteLine(transaction.Balance);
        }
    }
}
