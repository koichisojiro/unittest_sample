﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LiveUnitTest
{
    public class BankTransaction
    {
        public string Name { get; set; }
        public float Balance { get; set; }
        public BankTransaction(string accountName, float openingBalance)
        {
            this.Name = accountName;
            this.Balance = openingBalance;
        }
        public void Debit(float amount)
        {
            if (amount > Balance)
            {
                throw new ArgumentOutOfRangeException("amount");
            }
            if (amount < 0)
            {
                throw new ArgumentOutOfRangeException("amount");
            }
            Balance -= amount;
        }
        public void Crebit(float amount)
        {
            if (amount > Balance)
            {
                throw new ArgumentOutOfRangeException("amount");
            }
            if (amount < 0)
            {
                throw new ArgumentOutOfRangeException("amount");
            }
            Balance += amount;
        }
    }
}
