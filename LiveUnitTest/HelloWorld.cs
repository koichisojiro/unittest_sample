﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LiveUnitTest
{
    public class HelloWorld
    {
        public string Name { get; set; }
        public HelloWorld(string name)
        {
            this.Name = name;
        }
        public string Greetings()
        {
            return $"Hello {Name}, Welcome to Unit Test Training.";
        }
    }
}
